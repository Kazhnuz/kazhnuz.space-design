# kazhnuz.space Design

Juste un petit design fait pour mon blog, que je partage ici sous licence CC BY-SA

## Credits

- [UI Pack](http://opengameart.org/content/ui-pack) par Kenney

- [UI Pack RPG Extension](http://opengameart.org/content/ui-pack-rpg-extension) par Kenney

- [LPC Tile Atlas](http://opengameart.org/content/lpc-tile-atlas) par les contributeurs du projet Liberated Pixel Cup